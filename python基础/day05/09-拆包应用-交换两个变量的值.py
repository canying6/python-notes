a = 10
b = 20

# 方法一:
# c = a.txt
# a.txt = b
# b = c
# print(a.txt, b)

# 方法二: +/-   */÷
# a.txt = a.txt + b  # a.txt 30
# b = a.txt - b  # 10
# a.txt = a.txt - b  # 20
# print(a.txt, b)

# 方法三, python中的使用 组包和拆包
a, b = b, a
print(a, b)
