# 一个学校，有3个办公室，现在有8位老师等待工位的分配，请编写程序完成
import random

# 1.定义学校列表
schools = [[], [], []]
# 2. 定义老师列表
teachers = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']
# 3. 让老师抓阄
# 3.1 遍历老师列表
for teacher in teachers:
    # 3.2 抓阄，产生随机数
    num = random.randint(0, 2)  # 产生的随机数，相当于是办公室的下标
    # 3.3 老师进入办公室，将老师名字添加到办公室列表中
    schools[num].append(teacher)

print(schools)
for office in schools:
    print(f'该办公室老师的个数为{len(office)},办公室老师的名字为：')
    for teacher in office:
        print(teacher, end=' ')
    print()
