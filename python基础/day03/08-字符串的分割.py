my_str = 'hello world canying and canyingapp'

# my_str.split(sub_str, count)  将my_str 字符串按照sub_str 进行切割
# sub_str: 按照什么内容切割字符串，默认是空白字符，空格，tab建
# count: 切割几次，默认是全部切割
# 返回值： 列表 []

result = my_str.split()  # 按照空白字符，全部切割
print(result)

print(my_str.split('canying'))
print(my_str.split('canying', 1))  # 切割一次
print(my_str.rsplit('canying', 1))
